import React, { useRef } from "react";

export default function CompanyJobBoard({
	company: {
		frontmatter: { job_board_hostname, job_board_provider },
	},
}) {
	const jobList = useRef(null);
	if (job_board_provider && job_board_hostname) {
		return (
			<joblist-board
				ref={jobList}
				provider-name={job_board_provider}
				provider-hostname={job_board_hostname}
			></joblist-board>
		);
	} else {
		return (
			<p class="text text__info">
				Job positions cannot automatically be retrieved
			</p>
		);
	}
}
