import React from "react";

function sortIndex(a, b) {
	return a > b;
}

export default function IndexToc({ index }) {
	return Object.keys(index)
		.sort(sortIndex)
		.map((indexLetter) => {
			return (
				<li className="nav__item" key={indexLetter}>
					<a href={`#index-${indexLetter}`}>{indexLetter}</a>
				</li>
			);
		});
}
