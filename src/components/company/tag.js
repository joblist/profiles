import React from "react"
import {Link} from "gatsby"

export default function CompanyTag({
	tag
}) {
	return (
		<Link to={`/tags/companies/${tag}`} className="tag">
			{tag}
		</Link>
	)
}
