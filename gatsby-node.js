const { resolve } = require("path");
const { createFilePath } = require("gatsby-source-filesystem");

/* absolute imports
   https://www.gatsbyjs.com/docs/how-to/custom-configuration/add-custom-webpack-config/#absolute-imports
 */
/* exports.onCreateWebpackConfig = ({ stage, actions }) => {
 * 	actions.setWebpackConfig({
 * 		resolve: {
 * 			modules: [path.resolve(__dirname, "src"), "node_modules"],
 * 		},
 * 	})
 * } */

const getAllCompaniesTags = (companies) => {
	let allTags = [];
	companies.forEach(({ node: { frontmatter } }) => {
		const { tags } = frontmatter;
		tags &&
			tags.forEach((tag) => {
				allTags.indexOf(tag) === -1 ? allTags.push(tag) : null;
			});
	});
	return allTags;
};

const addSlugFieldToMarkdown = async ({ node, getNode, actions }) => {
	const { createNodeField } = actions;
	const filepaths = createFilePath({ node, getNode, basePath: `pages` })
		.split("/")
		.filter((pathElement) => pathElement);
	const slugPath = filepaths[filepaths.length - 1];
	await createNodeField({
		node,
		name: `slug`,
		value: slugPath,
	});
};

const queryAllCompanies = `
		{
			companies: allMarkdownRemark(
				filter: {
					fileAbsolutePath: {
						regex: "//(companies)/"
					}
				}
			) {
				edges {
					node {
						frontmatter {
							tags
						}
						fields {
							slug
						}
					}
				}
			}
		}
`;

const createCompanyPages = async ({ graphql, actions }) => {
	const { createPage } = actions;
	const result = await graphql(queryAllCompanies);
	if (!result || !result.data || !result.data.companies) return;

	return result.data.companies.edges
		.filter(({ node }) => {
			if (node && node.fields && node.fields.slug) return true;
		})
		.map(({ node }) => {
			const slug = node.fields.slug;
			return createPage({
				path: `companies/${slug}`,
				component: resolve(`src/components/company/page-template.js`),
				context: { slug },
			});
		});
};

const createCompaniesTagsPages = async ({ graphql, actions }) => {
	const { createPage } = actions;
	const {
		data: {
			companies: { edges: companies },
		},
	} = await graphql(queryAllCompanies);
	if (!companies) return;

	/* create a page for each tag, with all compnies with this tag */
	let allTags = getAllCompaniesTags(companies);

	allTags &&
		allTags.forEach((tag) => {
			createPage({
				path: `tags/companies/${tag}`,
				component: resolve(
					`src/components/tags/companies/page-template-tag.js`,
				),
				context: {
					slug: tag,
				},
			});
		});
};

exports.onCreateNode = async (props) => {
	const { node } = props;
	if (node.internal.type != `MarkdownRemark`) return;
	await addSlugFieldToMarkdown(props);
};

exports.createPages = async (props) => {
	await Promise.all([createCompanyPages(props)]);
	await Promise.all([createCompaniesTagsPages(props)]);
};
